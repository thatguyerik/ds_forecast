<?php

/**
 * @file
 * Administration callbacks for the Dark Sky Forecast module
 */

/**
 * Form builder. Configure Dark Sky.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function ds_forecast_admin_settings() {

  $form['ds_forecast_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => variable_get('ds_forecast_api_key', ''),
    '#description' => t('Your API key from https://developer.forecast.io'),
    '#weight' => 0,
  );

  $form['ds_forecast_geo_lat'] = array(
    '#type' => 'textfield',
    '#title' => t('Latitude'),
    '#default_value' => variable_get('ds_forecast_geo_lat', '31.311556'),
    '#weight' => 10,
  );

  $form['ds_forecast_geo_long'] = array(
    '#type' => 'textfield',
    '#title' => t('Longitude'),
    '#default_value' => variable_get('ds_forecast_geo_long', '-92.445070'),
    '#weight' => 20,
  );

  $form['ds_forecast_period'] = array(
    '#type' => 'select',
    '#title' => t('Forecast Period'),
    '#default_value' => variable_get('ds_forecast_period', 'currently'),
    '#weight' => 30,
    '#options' => array(
      'currently' => t('Current'),
      'minutely' => t('Minutely'),
      'hourly' => t('Hourly'),
      'daily' => t('Daily'),
    ),
  );

  $form['ds_forecast_cache_lifetime'] = array(
    '#type' => 'textfield',
    '#title' => t('Cache Lifetime (in seconds)'),
    '#description' => t('Leave blank or 0 to disable caching. Disable with caution. Minimum recommended value is 90.'),
    '#default_value' => variable_get('ds_forecast_cache_lifetime', '0'),
    '#weight' => 40,
    '#element_validate' => array('_validate_cache_lifetime'),
  );

  $form['#submit'][] = 'ds_forecast_admin_settings_submit';

  return system_settings_form($form);

}

function _validate_cache_lifetime($element, &$form_state) {
  if (!empty($element['#value']) && (!is_numeric($element['#value']) || intval($element['#value']) != $element['#value'] || $element['#value'] < 0)) {
    form_error($element, t('The "!name" option must be zero or a positive integer value.', array('!name' => t($element['#title']))));
  }
}

/**
 * Process Dark Sky Forecast settings update.
 */
function ds_forecast_admin_settings_submit($form, $form_state) {

  // Set variables for all settings.
  variable_set('ds_forecast_api_key', $form_state['values']['ds_forecast_api_key']);
  variable_set('ds_forecast_geo_lat', $form_state['values']['ds_forecast_geo_lat']);
  variable_set('ds_forecast_geo_long', $form_state['values']['ds_forecast_geo_long']);
  variable_set('ds_forecast_period', $form_state['values']['ds_forecast_period']);
  variable_set('ds_forecast_cache_lifetime', $form_state['values']['ds_forecast_cache_lifetime']);
  
  // If the cache lifetime value is 0, we will not be caching at all. Clear
  // any cached data that may exist. No new data will be cached unless the 
  // lifetime setting is changed to a positive integer.
  if ($form_state['values']['ds_forecast_cache_lifetime'] == 0) {
    cache_clear_all('ds_forecast:block', 'cache');
  }
  
}