<?php
/**
 * @file
 * This file is the default theme template for displaying forecast
 * data. Variables included for use in this file are:
 *
 * $time: A UNIX timestamp representing the time of the forecast
 * $summary: A brief text summary of the current weather conditions
 * $temperature: A floating point numerical value representing the current temperature
 * $icon: A token representing the current forecast that can be used as a CSS class name
 * $precipProbability: A numerical value between 0 and 1 representing the probability
 *    of precipitation occurring during the current forecast period
 * $precipIntensity: A numerical value representing the average expected
 *    intensity (in inches of liquid per hour) of precipitation occurring at
 *    the given time, conditional on probability (assuming any precipitation
 *    occurs at all)
 * $alerts: An array of severe weather alerts in effect during the current
 *    forecast period.
 */
?>

<?php if ($period): ?>
  <div class="ds-forecast">
  
    <i class="wi wi-forecast-io-<?php print $icon; ?>"></i>
    <span class="forecast-summary">
      <?php print $summary; ?>
      <?php if ($temperature): print ($period == 'currently' ? '' : 'Currently ') . round($temperature) . '&deg;'; endif; ?>
    </span>
    <?php if ($alerts): ?>
      <?php foreach ($alerts as $alert): ?>
        |
        <span class="forecast-alert">
          <a href="<?php print $alert['uri']; ?>" target="_blank"><?php print $alert['title']; ?></a>
        </span>
      <?php endforeach; ?>
    <?php endif; ?>
  
  </div>
<?php endif; ?>